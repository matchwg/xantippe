# Xantippe

Xantippe is a Spring application for file auto-completion & management.

## Usage

Use Java 17 or [Apache Tomcat 10](https://tomcat.apache.org/download-10.cgi) to run [the Java Achieve](https://gitlab.com/matchwg/xantippe/-/releases) file.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[GNU AGPL](https://www.gnu.org/licenses/lgpl-3.0.html)