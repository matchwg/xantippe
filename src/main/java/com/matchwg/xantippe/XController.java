package com.matchwg.xantippe;

import java.io.FileNotFoundException;
import java.util.UUID;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.matchwg.xantippe.db.FileTypeRepository;
import com.matchwg.xantippe.define.XElementProperties;
import com.matchwg.xantippe.define.XFile;
import com.matchwg.xantippe.define.XFile.XFileType;
import com.matchwg.xantippe.define.XFileTypeConverter;
import com.matchwg.xantippe.define.XUser;
import com.matchwg.xantippe.define.XUtil;
import com.matchwg.xantippe.define.XUser.UserType;

@SessionAttributes("isBrowser")
@Controller
public class XController {
	/**
	 * The code identify the server. TODO to write in database.
	 */
	private static final String serverSecurityCode = UUID.randomUUID().toString().split("-", 0)[0];
	public static final XUser user = new XUser(UserType.admin, "Match123");
	
	private XFileTypeConverter xftc;
	
	public XController(FileTypeRepository fileTypeRepository) {
		this.xftc = new XFileTypeConverter(fileTypeRepository);
	}
	
	/**
	 * A simple home request handler.
	 * @author match123
	 * @return the view at "src/main/resources/templates/home.html"
	 * @throws FileNotFoundException when testing a input stream
	 */
	@GetMapping("/")
	public String home(
			@RequestParam(value = "isBrowser",defaultValue = "true")String isBrowser,
			Model model,
			RedirectAttributes redirectAttributes) throws FileNotFoundException {
		model.addAttribute("isBrowser",isBrowser);
		redirectAttributes.addAttribute("isBrowser",isBrowser);
		//Timestamp timestamp = Timestamp.from(Instant.now());
		//String timesString = String.valueOf(timestamp.getTime());
		//TODO DEBUG
		XFile file = new XFile();
		file.init(XFileType.application,serverSecurityCode,"Goods",xftc,user);
		XElementProperties tmp = file.getProperties();
		
		//model.addAttribute("time", serverSecurityCode+" "+file.getFileID());
		//long l1 = System.currentTimeMillis();
		//model.addAttribute("uuid", XFile.checkSearchCode("3762a47b", "16877897057036BA"));
		//model.addAttribute("stamp", XUser.calculateUserCode(xUser, serverSecurityCode));
		model.addAttribute("stamp", tmp.getETimestamp().getTime()+"-"+file.getSearchCode()+"-"+tmp.getETimestamp());
		//model.addAttribute("time", System.currentTimeMillis()-l1);
		//model.addAttribute("time",serverSecurityCode+"-"+file.getEUID());
		model.addAttribute("time",XUtil.encodeTimestampHex(file.getProperties().getETimestamp().getTime()));
		//model.addAttribute("uuid",XFile.checkSearchCode(serverSecurityCode, file.getEUID()));
		//model.addAttribute("uuid", XFile.calculateCorrectionCodeD(serverSecurityCode, "Match123", new FileInputStream("src/main/resources/static/img/server-icon.png")));
		return "home";
	}
	
	@GetMapping("/barcodePrint")
	public String barcode(ModelMap model,
			@RequestParam(value = "type",defaultValue = "DATA_MATRIX") String type,
			@RequestParam(value = "type2",defaultValue = "CODE128") String type2,
			@RequestParam(value = "content",defaultValue = "1234567") String content,
			@RequestParam(value = "isBrowser",defaultValue = "true")String isBrowser,
			RedirectAttributes redirectAttributes) {
		
		String src = BarcodeController.barcode(content, type, "true");
		String src2 = BarcodeController.barcode(content, type2, "true");
		model.addAttribute("isBrowser",isBrowser);
		redirectAttributes.addAttribute("isBrowser", isBrowser);
		model.addAttribute("content", content);
		model.addAttribute("type",type);
		model.addAttribute("src",src);
		model.addAttribute("src2",src2);
		
		return "barcode";
	}
	
	protected static String getSecurityCode() {
		return serverSecurityCode;
	}

}


