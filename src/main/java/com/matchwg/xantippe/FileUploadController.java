package com.matchwg.xantippe;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.matchwg.xantippe.db.FileTypeRepository;
import com.matchwg.xantippe.define.XElementProperties;
import com.matchwg.xantippe.define.XFile;
import com.matchwg.xantippe.define.XFileTypeConverter;
import com.matchwg.xantippe.define.XFolder;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/file/upload")
@Slf4j
@SessionAttributes({"folder","isBrowser"})
public class FileUploadController {
	private XFileTypeConverter xftc;
	
	private final FileTypeRepository fileTypeRepository;
	
	public FileUploadController(FileTypeRepository fileTypeRepository) {
		this.fileTypeRepository = fileTypeRepository;
		this.xftc = new XFileTypeConverter(fileTypeRepository);
	}
	
	/*
	private Iterable<XFile> filterByType(List<XFile> files, XFileType type) {
		return files.stream()
					.filter(t -> ((XFileType)(t.getEType())).equals(type))
					.collect(Collectors.toList());
	}
	*/
	@ModelAttribute
	public void addTypeToModel(Model model) {
/*		List<XFile> files = new ArrayList<XFile>();
		files.add(new XFile("A", "application", XFileType.application));
		files.add(new XFile("B", "audio", XFileType.audio));
		files.add(new XFile("C", "font", XFileType.font));
		files.add(new XFile("D", "image", XFileType.image));
		files.add(new XFile("E", "message", XFileType.message));
		files.add(new XFile("F", "model", XFileType.model));
		files.add(new XFile("G", "multipart", XFileType.multipart));
		files.add(new XFile("H", "text", XFileType.text));
		files.add(new XFile("I", "video", XFileType.video));
		files.add(new XFile("X", "undefined", XFileType.undefined));

		System.out.println(files.toString());
		model.addAttribute("files",files);
*/
		Iterable<XFile> xFiles =fileTypeRepository.findAll();
		model.addAttribute("files", xFiles);
	}
	
	@ModelAttribute(name = "folder")
	public XFolder folder() {return new XFolder();}
	
	@ModelAttribute(name = "file")
	public XFile file() {return new XFile();}
	
	@ModelAttribute(name = "properties")
	public XElementProperties properties() {return new XElementProperties(null, 0, null, null);}
	
	@GetMapping
	public String showUploadForm(@RequestParam(value = "isBrowser",defaultValue = "true")String isBrowser,
			Model model) {
			model.addAttribute("isBrowser",isBrowser);
			return "upload";
		}
	
	@PostMapping
	public String processFile(
			@Valid XFile file, 
			Errors errors,
			@RequestParam(value = "isBrowser",defaultValue = "true") String isBrowser, 
			  XFolder folder, 
			Model model,
			RedirectAttributes redirectAttributes) {
		
		model.addAttribute("isBrowser",isBrowser);
		model.addAttribute("errors",errors);
		redirectAttributes.addAttribute("isBrowser", isBrowser);
		System.out.println("ERRORSSSSSSSSSSSSSSSSS"+errors.getAllErrors());
		if(errors.hasErrors())return "upload";
		
		System.out.println(file.getProperties().getEName()+"--"
				//+file.getTypeName().toString()+"--"
				//+file.getCodeC().toString()+"--"
				+file.getData().getContentType()+"--"
				//+file.getType().toString()+"--"
				+file.getProperties().getEType());
		
		file.init(xftc.convert(file.getProperties().getETypeName()),
				XController.getSecurityCode(), file.getProperties().getEName(), xftc, XController.user);
		if(model.getAttribute("folder") instanceof XFolder) {
			folder = (XFolder)model.getAttribute("folder");
		}
		folder.addFile(file);
		model.addAttribute("folder",folder);  
		System.out.println(folder.toString());
		log.info("Processing file:", file);
		
		return "redirect:/folder/current";
	}
	
}
