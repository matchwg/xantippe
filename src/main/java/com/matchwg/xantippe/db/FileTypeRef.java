package com.matchwg.xantippe.db;

import lombok.Data;

@Data
public class FileTypeRef {
	private final String fileType;
}
