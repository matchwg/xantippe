package com.matchwg.xantippe.db;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;

import javax.sql.rowset.serial.SerialException;

import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.matchwg.xantippe.define.XElementProperties;
import com.matchwg.xantippe.define.XFile;
import com.matchwg.xantippe.define.XFolder;
import com.matchwg.xantippe.define.XUtil;

@Repository
public class JdbcFolderRepository implements FolderRepository{
	private JdbcOperations jdbcOperations;
	public JdbcFolderRepository(JdbcOperations jdbcOperations) {
		this.jdbcOperations = jdbcOperations;
	}
	@Override
	public XFolder save(XFolder folder) {
		PreparedStatementCreatorFactory pscf = new PreparedStatementCreatorFactory(
				"insert into Folder (ename, ealisa, ebool, etime) "
				+ "values (?,?,?,?)",
				Types.VARCHAR, Types.VARCHAR, Types.TINYINT, Types.TIMESTAMP);
		pscf.setReturnGeneratedKeys(true);
		XElementProperties properties = folder.getProperties();
		properties.setETimestamp(XUtil.getTimeNow());
		PreparedStatementCreator psc = pscf.newPreparedStatementCreator(
				Arrays.asList(
						properties.getEName(),
						properties.getEAlisa(),
						XUtil.booleanPropToByte(properties),
						properties.getETimestamp()));
		
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcOperations.update(psc, keyHolder);
		long folderId = keyHolder.getKey().longValue();
		properties.setEUID(folderId);
		
		List<XFile> files = folder.getFiles();
		int i = 0;
		for (XFile xFile : files) {
			saveFile(folderId, i++, xFile);
		}
		return folder;
	}
	
	private long saveFile(Long folderId, int orderKey, XFile file) {
		XElementProperties properties = file.getProperties();
		if(properties.getETimestamp() == null) properties.setETimestamp(XUtil.getTimeNow());
		PreparedStatementCreatorFactory pscf = new PreparedStatementCreatorFactory(
				"insert into File (folder, folder_key, ename, ealisa, edesc, ebool, etime, data) " 
				+ "values (?,?,?,?,?,?,?,?)",
				Types.BIGINT, Types.BIGINT, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR,
				Types.TINYINT, Types.TIMESTAMP, Types.BLOB);
		pscf.setReturnGeneratedKeys(true);
		PreparedStatementCreator psc = null;
		try {
			psc = pscf.newPreparedStatementCreator(Arrays.asList(
					folderId,
					orderKey,
					properties.getEName(),
					properties.getEAlisa(),
					properties.getEDescription(),
					XUtil.booleanPropToByte(properties),
					properties.getETimestamp(),
					file.getBlobByResource()));
		} catch (SerialException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcOperations.update(psc, keyHolder);
		long fileId = keyHolder.getKey().longValue();
		properties.setEUID(fileId);
		saveXFileTypeRefs(fileId, List.of(new FileTypeRef(properties.getETypeName())));
		return fileId;
	}
	
	private void saveXFileTypeRefs(long fileId, List<FileTypeRef> fileTypeRefs) {
		int key = 0;
		for (FileTypeRef fileTypeRef : fileTypeRefs) {
			jdbcOperations.update("insert into XFileType_Ref (xfiletype, file, file_key) "
					+"values (?,?,?)",
					fileTypeRef.getFileType(),fileId,key++);
		}
	}
}
