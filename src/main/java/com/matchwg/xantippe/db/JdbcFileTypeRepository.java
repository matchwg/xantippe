package com.matchwg.xantippe.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.matchwg.xantippe.define.XFile;
import com.matchwg.xantippe.define.XFile.XFileType;

@Repository
public class JdbcFileTypeRepository implements FileTypeRepository{
	
	private JdbcTemplate jdbcTemplate;
	
	public JdbcFileTypeRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	private XFile mapRowtoXFile(ResultSet row, int rowNum) throws SQLException {
		return new XFile(row.getString("id"), 
				row.getString("name"), 
				XFileType.valueOf(row.getString("type")));
	}
	
	@Override
	public Iterable<XFile> findAll() {
		return jdbcTemplate.query("select id, name, type from XFileType", this::mapRowtoXFile);
	}

	@Override
	public Optional<XFile> findById(String id) {
		List<XFile> results = jdbcTemplate.query("select id, name, type from XFileType where id = ?", 
				this::mapRowtoXFile, 
				id);
		return results.size() == 0 ? Optional.empty() : Optional.of(results.get(0));
	}

	@Override
	public XFile save(XFile xFile) {
		jdbcTemplate.update("insert into XFileType (id, name, type) values (?, ?, ?)", 
				xFile.getCodeC(), xFile.getTypeName(), xFile.getType().toString());
		return xFile;
	}
	
}
