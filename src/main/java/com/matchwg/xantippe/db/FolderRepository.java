package com.matchwg.xantippe.db;

import com.matchwg.xantippe.define.XFolder;

public interface FolderRepository {
	XFolder save(XFolder folder);
}
