package com.matchwg.xantippe.db;

import java.util.Optional;

import com.matchwg.xantippe.define.XFile;

public interface FileTypeRepository {
	Iterable<XFile> findAll();
	Optional<XFile> findById(String id);
	XFile save(XFile xFile);
}
