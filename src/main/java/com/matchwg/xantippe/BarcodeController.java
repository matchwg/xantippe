package com.matchwg.xantippe;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.zxing.common.BitMatrix;
import com.google.zxing.datamatrix.encoder.SymbolShapeHint;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;


/**
 * @author match123
 * A controller designed to handle the barcode request.
 * The program looks like this: 
 * 		content -> (zxing core's Writer) Matrix -> (zxing javase) BufferedImage
 * 				-> ByteArray -> (commons-codec) Base64 -> String src -> view
 * @version zxing 3.5.1
 * @version commons-codec 1.16.0
 * @version java 17
 */
@RestController
public class BarcodeController {
	
	/*
	//generate-model
	public static BufferedImage generateCode128(String content) {
		//Length = 11*character amount + 35 in Code128 A&B 
		BitMatrix matrix = new Code128Writer().encode(content, BarcodeFormat.CODE_128, 11*content.length()+35, 80); 
		return MatrixToImageWriter.toBufferedImage(matrix);
	}
	//TODO modern size controller
	public static BufferedImage generateQRCode(String content) throws WriterException {
		BitMatrix matrix = new QRCodeWriter().encode(content, BarcodeFormat.QR_CODE, 177, 177);
		return MatrixToImageWriter.toBufferedImage(matrix);
	}
	public static BufferedImage generateDataMatrix(String content) {
		BitMatrix matrix = new DataMatrixWriter().encode(content, BarcodeFormat.DATA_MATRIX, 52, 52);
		return MatrixToImageWriter.toBufferedImage(matrix);
	}
	*/
	
	
	/**
	 * @author match123
	 * see com.google.zxing.MultiFormatWriter#encode(String, BarcodeFormat, int, int, Map)
	 * @param content 
	 * @param bFormat
	 * @return BufferedImage
	 * 
	 * Pass parameter to encoder with these specified configurations:
	 * 	* Use UTF-8 for encoding
	 * 	* Only Code128, Data Matrix and QR Code are available (see: todo)
	 * 	* Only use Code128 B character set
	 * 	* Sizes are all 80*80 (Code128 follows: the width = 11*content length + 35, which is used
	 * 		to calculate the length of Code128 A/B)
	 * 	* The shape of Data Matrix is (force) square
	 * 	* The error correction level of QR Code is set as H
	 * @throws WriterException
	 */
	public static BufferedImage generateBufferedImage(String content, BarcodeFormat bFormat) throws WriterException {
		int width = 80;
		int height = 80;
		
		Map<EncodeHintType,Object> hints = new HashMap<>();
		
		hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
		
		switch (bFormat) {
		case CODE_128: {
			hints.put(EncodeHintType.FORCE_CODE_SET, "B");
			//Length = 11*character amount + 35 in Code128 A&B 
			width = 11*content.length()+35;
			break;
		}
		case DATA_MATRIX:{
			hints.put(EncodeHintType.DATA_MATRIX_SHAPE, SymbolShapeHint.FORCE_SQUARE);
			break;
		}
		case QR_CODE:{
			hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
			break;
		}
		default:
			throw new IllegalArgumentException("Unexpected value: " + bFormat);
		}
		
		BitMatrix matrix = new MultiFormatWriter().encode(content, bFormat, width, height,hints);
		return MatrixToImageWriter.toBufferedImage(matrix);
	}
	
	/**
	 * @author match123
	 * The program looks like: BufferedImage --(ImageIO)-> ByteArrayOutputStream 
	 * 	--(encodeToString())-> String base64
	 * @param bImage BufferedImage
	 * @return the string in base64
	 * @throws IOException
	 */
	public static String imageToBase64(BufferedImage bImage) throws IOException {
		ByteArrayOutputStream oStream = new ByteArrayOutputStream();
		ImageIO.write(bImage, "png", oStream);
		return new Base64().encodeToString(oStream.toByteArray());
	}
	
	public static String barcode(String content, String type, String isBrowser) {
		BarcodeFormat bFormat;
		String Base64 = "barcode";
		try {
			bFormat = BarcodeFormat.valueOf(type);
		} catch (Exception e) {
			bFormat = BarcodeFormat.CODE_128;
			// TODO: handle exception
		}
		BufferedImage bufferedImage = null;
		switch (bFormat) {
		case CODE_128,QR_CODE,DATA_MATRIX: 
			try {
				bufferedImage = generateBufferedImage(content,bFormat);
			} catch (WriterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		default:
			throw new IllegalArgumentException("Unexpected value: " + bFormat.toString());
		}
		
		
		try {
			Base64 = imageToBase64(bufferedImage);
			if(isBrowser.equals("true")) Base64 = "data:image/png;base64,".concat(Base64);
			//model.put("src", "data:image/png;base64,".concat(Base64));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			//model.put("src", "");
		} finally {
			//model.put("content", content);
		}
		//if(isBrowser.equals("true")) return "barcode";
		return Base64;
	}
	
	/**
	 * Generate and pass the barcode to the view "barcode.html".
	 * @author match123
	 * @param model data-pass channel
	 * @param content
	 * @param type the BarcodeFormat
	 * @return the view at "resource/templates/barcode.html"
	 */
	@GetMapping(value = "/barcode")
	public String barcodeController(ModelMap model,
				@RequestParam(value = "content",defaultValue = "1234567") String content,
				@RequestParam(value = "type",defaultValue = "DATA_MATRIX") String type,
				@RequestParam(value = "browser",defaultValue = "false") String isBrowser) {
		//TODO every type of BarcodeFormat
		model.addAttribute("isBrowser",isBrowser);
		return barcode(content, type, isBrowser);
	}
}
