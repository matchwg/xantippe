package com.matchwg.xantippe;

import java.io.IOException;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.matchwg.xantippe.define.XFile;
import com.matchwg.xantippe.define.XFolder;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/file/download")
@Slf4j
@SessionAttributes("folder")
public class DownloadController {
	@GetMapping
    public ResponseEntity<Resource> downloadFile(@ModelAttribute("folder") XFolder folder, 
    		@RequestParam("euid") String euid) throws IOException {
    	XFile file = folder.selectFileByEUID(euid);
    	System.out.println(file.getData().getSize());
    	byte[] bytes;
		try {
			bytes = file.getResource().getContentAsByteArray();
			if(bytes == null || bytes.length == 0) return null;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}
    	InputStreamResource resource = new InputStreamResource(file.getResource().getInputStream());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Content-Disposition", String.format("attachment; filename="+file.getProperties().getEName())); 
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Content-Language", "UTF-8");
        log.info("Processing resource:",file);
        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(bytes.length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
	}
}
