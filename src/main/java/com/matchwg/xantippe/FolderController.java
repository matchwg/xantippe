package com.matchwg.xantippe;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.matchwg.xantippe.db.FolderRepository;
import com.matchwg.xantippe.define.XFolder;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
@Slf4j
@Controller
@RequestMapping("/folder")
@SessionAttributes(value = {"folder","isBrowser"})
public class FolderController {
	private FolderRepository folderRepository;
	
	public FolderController(FolderRepository folderRepository) {
		this.folderRepository = folderRepository;
	}
	
	@GetMapping("current")
	public String folderForm(
			@RequestParam(value = "isBrowser") String isBrowser, 
			@ModelAttribute("isBrowser") String is2,
			Model model,
			RedirectAttributes redirectAttributes) {
		if (isBrowser.isEmpty()) {
			if (is2.isEmpty()) {
				model.addAttribute("isBrowser","true");
			}
			model.addAttribute("isBrowser",is2);
			
		}
		return "folder";
	}
	
	@PostMapping
	public String processFolder(
			@Valid XFolder folder, 
			Errors errors, 
			SessionStatus sessionStatus,
			@ModelAttribute("isBrowser") String isBrowser,
			Model model,
			RedirectAttributes redirectAttributes) {
		System.out.println(model.getAttribute("errors")+"**********");
		model.addAttribute("errors", errors);
		System.out.println(model.getAttribute("errors")+"**********");
		System.out.println(errors);
		model.addAttribute(isBrowser);
		redirectAttributes.addAttribute("isBrowser",isBrowser);
		if(errors.hasErrors())return "folder";
		folderRepository.save(folder);
		log.info("Folder summitted: ", folder);
		
		sessionStatus.setComplete();
		
		return "redirect:/";
	}
}
