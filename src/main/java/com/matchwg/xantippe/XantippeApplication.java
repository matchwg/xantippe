package com.matchwg.xantippe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
/**
 * @author match123
 * A starter created by Spring.
 */
@SpringBootApplication
public class XantippeApplication implements WebMvcConfigurer {
	public static void main(String[] args) {

		SpringApplication.run(XantippeApplication.class, args);
	}
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		// TODO Auto-generated method stub
		registry.addViewController("/info").setViewName("info");
	}
}
