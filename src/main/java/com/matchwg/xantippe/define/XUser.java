package com.matchwg.xantippe.define;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import lombok.Data;

@Data
public class XUser{
	
	//TODO JDBC Repository
	private String userActiveCode;
	private List<XFolder> folders = new ArrayList<>();
	private XElementProperties properties;
	
	
	public XUser(UserType type, String name) {
		properties = new XElementProperties(
				name, 
				UUID.randomUUID().getMostSignificantBits(), 
				type, 
				Timestamp.from(Instant.now()));
		folders.add(new XFolder());
	}
	
	//DB Helper
	public XUser(XElementProperties properties,
			String userActiveCode, List<XFolder> folders) {
		this.properties = properties;
		this.userActiveCode = userActiveCode;
		this.folders = folders;
		
	}
	
	public enum UserType{
		admin,helper,user,guest
	}
}
