package com.matchwg.xantippe.define;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.matchwg.xantippe.db.FileTypeRepository;
import com.matchwg.xantippe.define.XFile.XFileType;

@Component
public class XFileTypeConverter implements Converter<String, XFileType>{
	
	private FileTypeRepository fileTypeRepository;
	
	public XFileTypeConverter(FileTypeRepository fileTypeRepository) {
		this.fileTypeRepository = fileTypeRepository;
	}
	
	@Override
	public XFileType convert(String source) {
		// TODO Auto-generated method stub
		return fileTypeRepository.findById(source).orElse(null).getType();
	}
	
	public XFileType convert(char c) {
		return convert(String.valueOf(c));
	}
	
	public String deConvert(XFileType xType) {
		Iterable<XFile> files = fileTypeRepository.findAll();
		for (XFile xFile : files) {
			if(xFile.getType().equals(xType)) {
				return xFile.getCodeC();
			}
		}
		return "X";
	}
	public char deConvert2(XFileType type) {
		return deConvert(type).charAt(0);
	}
	/*
	public static XFileType IdConventer(char id) {
		switch (id) {
		case 'A': return XFileType.application;
		case 'B': return XFileType.audio;
		case 'C': return XFileType.font;
		case 'D': return XFileType.image;
		case 'E': return XFileType.message;
		case 'F': return XFileType.model;
		case 'G': return XFileType.multipart;
		case 'H': return XFileType.text;
		case 'I': return XFileType.video;
		default:  return XFileType.undefined;
		}
	}
	
	public static char XTypeConventer(XFileType xType) {
		switch (xType) {
		case application: return 'A';
		case audio: return 'B';
		case font: return 'C';
		case image: return 'D';
		case message: return 'E';
		case model: return 'F';
		case multipart: return 'G';
		case text: return 'H';
		case video: return 'I';
		default:  return 'X';
		}
	}
	*/
	/*
	private Map<String, XFileType> typeMap = new HashMap<>();
	
	public XFileTypeConverter() {
		typeMap.put("A", XFileType.application);
		typeMap.put("B", XFileType.audio);
		typeMap.put("C", XFileType.font);
		typeMap.put("D", XFileType.image);
		typeMap.put("E", XFileType.message);
		typeMap.put("F", XFileType.model);
		typeMap.put("G", XFileType.multipart);
		typeMap.put("H", XFileType.text);
		typeMap.put("I", XFileType.video);
		typeMap.put("X", XFileType.undefined);
	}

	@Override
	public XFileType convert(String source) {
		// TODO Auto-generated method stub
		return typeMap.get(source);
	}
	
	public XFileType convert(char source) {
		return typeMap.get(String.valueOf(source));
	}
	
	public String deConvert(XFileType type){
		return typeMap.entrySet()
				.stream()
				.filter(n -> type.equals(n.getValue()))
				.toList()
				.get(0)
				.getKey();
	}

	*/
}
