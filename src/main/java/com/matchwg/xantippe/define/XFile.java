package com.matchwg.xantippe.define;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Data;


/**
 * File object -- the core conception of this program.
 * It comes with these features:
 * * has a File ID (default: timeStamp+codeA+codeB 15 bit)
 * * supports virtual file before creation
 * * 3 ErrorCodes (	A: includes fileStamp B: includes serverRandomCode & fileType
 * 					D: includes userID & [file etc.]InputStream)
 * 
 */
@Data
public class XFile {

	//private final String fileID;
	@Valid
	private XElementProperties properties;
	private char[] errorCodes;
	//private final byte codeA;
	//@NotNull(message = "File type is required")
	private String codeC;
	private String typeName;
	private XFileType type;
	@NotNull
	private MultipartFile data;
	private Resource resource;
	//private final Timestamp fileStamp;
	//private final XFileType fileType;
	private String searchCode;
	
	public XFile() {}
	
	//view helper
	public XFile(String id,String name,  XFileType type) {
		typeName = name;
		codeC = id;
		this.type = type;
	}
	
	//DB helper
	public XFile(XElementProperties properties, char[] errorCodes, Blob blob, String searchCode) throws IOException {
		this.properties = properties;
		this.errorCodes = errorCodes;
		try {
			this.setBlob(blob);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.searchCode = searchCode;
	}
	
	public void init(XFileType fileType, String serverCode, String fileName, XFileTypeConverter xftc, XUser user) {
		// make sure the nano part of Timestamp.toString() has three decimal places
		Timestamp tempTimestamp = new Timestamp(Timestamp.from(Instant.now()).getTime());
		properties = new XElementProperties(
				fileName,
				tempTimestamp.getTime(),
				fileType,
				tempTimestamp);
		this.errorCodes =  new char[4];
		this.errorCodes[0] = String.valueOf(XUtil.calculateCorrectionCodeA(this)).charAt(0);
		//System.out.println(eTimestamp.toString()+"--"+fileType.toString()+errorCodes[0]);
		this.errorCodes[1] = XUtil.calculateCorrectionCodeB(serverCode, this);
		this.errorCodes[2] = xftc.deConvert2(fileType);
		System.out.println("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");
		try {
			if(data != null) this.resource = new ByteArrayResource(data.getBytes());
			if(resource == null || resource.contentLength() == 0) throw new IOException();
			System.out.println(resource.getContentAsByteArray());
			this.errorCodes[3] = XUtil.calculateCorrectionCodeD(serverCode, user.getProperties().getEName(), resource.getContentAsByteArray());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			this.errorCodes[3] = 'X';
		}
		this.searchCode = XUtil.encodeTimestampHex(properties.getEUID()).concat(String.valueOf(errorCodes));
		
		//super.setETimestamp(fileStamp);
		//super.setEType(fileType);
		//super.setEUID(fileID);
		//System.out.println("THIS IS YOUR SOUT!!!!!!!!!!!!!"+XFileType.XTypeConventer(fileType));
	}
	
	public Blob getBlob() throws SerialException, SQLException, IOException {
		return new SerialBlob(data.getBytes());
	}
	
	public Blob getBlobByResource() throws SerialException, SQLException, IOException {
		return new SerialBlob(resource.getContentAsByteArray());
	}
	
	public void setBlob(Blob blob) throws SQLException, IOException {
		resource = new ByteArrayResource(blob.getBinaryStream().readAllBytes());
	}
	
	/**
	 * Make file filterable using MIMEType
	 * @author match123
	 */
	public enum XFileType {
		/*
		audio{
			@Override
			public String[] getFileType() {
				String[] alisas = {"mp3","ogg","flac","wav","wma","oga"};
				return alisas;
			}
			@Override
			public String getMIMEType() {
				// TODO Auto-generated method stub
				return "audio/*";
			}
		},
		
		image{
			@Override
			public String[] getFileType() {
				String[] alisas = {"jpg","jpeg","png","bmp","gif","tiff","webp"};
				return alisas;
			}
			@Override
			public String getMIMEType() {
				// TODO Auto-generated method stub
				return "image/*";
			}
		},
		video{
			@Override
			public String[] getFileType() {
				String[] alisas = {"mp4","ogv","webm","wmv"};
				return alisas;
			}
			@Override
			public String getMIMEType() {
				// TODO Auto-generated method stub
				return "video/*";
			}
		},
		document{
			@Override
			public String[] getFileType() {
				String[] alisas = {"ods","odt","odp","odg","ppt","doc","xls","pptx","docx","xlsx","pdf","mobi","epub"};
				return alisas;
			}
			
		},
		graph{
			@Override
			public String[] getFileType() {
				String[] alisas = {"xopp","mm","drawio","ggb","psd","kra","xcf","blend","m","sla"};
	 			return alisas;
			}
		},
		text{
			@Override
			public String[] getFileType() {
				String[] alisas = {"txt","md","json","xml","lyx","tex","rst","html","yaml"};
				return alisas;
			}
		},
		application{
			@Override
			public String[] getFileType() {
				String[] alisas = {"exe","dmg","deb","rpm","AppImage","jar","war","sh","bat","apk"};
				return alisas;
			}
		},
		archive{
			@Override
			public String[] getFileType() {
				String[] alisas = {"tar.gz","tar.xz","zip","7z","rar"};
				return null;
			}
		};

		public abstract String[] getFileType();
		public abstract String getMIMEType();
		*/
		

		/*
	    application("application/*",'A'),
	    audio("audio/*",'B'),
	    font("font/*",'C'),
	    image("image/*",'D'),
	    message("message/*",'E'),
	    model("model/*",'F'),
	    multipart("multipart/*",'G'),
	    text("text/*",'H'),
	    video("video/*",'I'),
		undefined("undefined",'X');
*/
	    application,
	    audio,
	    font,
	    image,
	    message,
	    model,
	    multipart,
	    text,
	    video,
		undefined;

		
		//private XFileType() {
			//mime = this.mime;
			//id = this.id;
		//}
		/*not work
		public char getId() {
			assert Character.isDefined(id);//passed :(
			return id;
		}
		public String getMime() {
			return mime;
		}
		*/
	}
	
}