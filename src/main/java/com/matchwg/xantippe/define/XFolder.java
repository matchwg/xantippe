package com.matchwg.xantippe.define;

import java.util.ArrayList;
import java.util.List;
import jakarta.validation.Valid;
import lombok.Data;

@Data
public class XFolder {	
	private List<XFile> files = new ArrayList<>();
	@Valid
	private XElementProperties properties;
	
	public void addFile(XFile file) {
		files.add(file);
	}
	
	//view helper
	public XFolder(){
	}
	
	public XFolder(List<XFile> files, XElementProperties properties) {
		this.files = files;
		this.properties = properties;
	}
	
	public XFile selectFileByEUID(String euid) {
		for (XFile xFile : files) {
			if (String.valueOf(xFile.getProperties().getEUID()).equals(euid)) {
				return xFile;
			}
		}
		return null;
	}
}
