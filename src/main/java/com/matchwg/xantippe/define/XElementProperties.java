package com.matchwg.xantippe.define;

import java.io.Serializable;
import java.sql.Timestamp;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

/**
 * Universal properties for users, files and actions.
 */
@Data
public class XElementProperties implements Serializable {
	private static final long serialVersionUID = 3723866208810733229L;
	
	@Size(min = 5, max = 50)
	@NotBlank
	private String eName;
	@Size(max = 50)
	private String eAlisa;
	
	@Size(max = 500)
	private String eDescription;
	private long eUID;
	private Enum<?> eType;
	@NotNull
	private String eTypeName;
	private boolean isExist = false;
	private boolean isOccupired = false;//for user, it's logged.
	private boolean isDestroyed = false;
	private boolean isHidden = false;
	private boolean isStashed = true;//for file, it's read-only; for user, it's disabled.
	private Timestamp eTimestamp;
	
	public XElementProperties() {
		// TODO Auto-generated constructor stub
	}
	
	public XElementProperties(String eName, long eUID, Enum<?> eType, Timestamp eTimestamp){
		this.eName = eName;
		this.eUID = eUID;
		this.eType = eType;
		this.eTimestamp = eTimestamp;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
