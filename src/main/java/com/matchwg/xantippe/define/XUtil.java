package com.matchwg.xantippe.define;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Random;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.util.Base64Util;

import com.matchwg.xantippe.db.FileTypeRepository;
import com.matchwg.xantippe.define.XFile.XFileType;

public class XUtil {


	/**
	 * Generate CodeA by doing like ISBN.(a little change: 
	 * 		1*a1+3*a2+1*a3+3*a4+1*a5+3*a6+1*a7+3*a8+1*a9
	 * 		+3*a10+1*a11+3*a12+1*a13+1*CodeA  % 10 == 0)
	 * I'm sure that CodeA is never larger than 9, so it's no need to use char.
	 * @author match123
	 * @param file includes basic information
	 * @return CodeA
	 */
	public static byte calculateCorrectionCodeA(XFile file) {
		String stamp = String.valueOf(file.getProperties().getETimestamp().getTime());
		assert stamp.length() == 13;
		boolean isOdd = true;
		int sum = 0;
			for(char c:stamp.toCharArray()) {
				if(isOdd) {
					sum+=Integer.valueOf(c);
					isOdd=!isOdd;
				}else {
					sum+=Integer.valueOf(c)*3;
					isOdd=!isOdd;
				}
			}
		byte mod =(byte)(sum % 10);
		byte result =(byte) (mod == 0?0:10-mod);
		return result;
	}
	/**
	 * Generate CodeB by sha384.
	 * @param serverRandomCode
	 * @param file that includes basic information.
	 * @return CodeB
	 */
	public static char calculateCorrectionCodeB(String code, XFile file) {
		String message = (file.getProperties().getETimestamp().toString()
				+((XFileType)file.getProperties().getEType()).name()
				+code);
		String result = DigestUtils.sha384Hex(message);
		System.out.println(message+"\r\n"+result+"\r\n"+Character.toUpperCase(result.charAt(0)));
		return Character.toUpperCase(result.charAt(0));
	}
	/**
	 * Generate CodeC by sha384(fileData),base64(Username),md5(serverRandomCode)
	 * 	 and sha3_224(All).
	 * @author match123
	 * @param serverCode serverRandomCode
	 * @param userid userid/username TODO
	 * @param in fileData
	 * @return char CodeC
	 */
	public static char calculateCorrectionCodeD(String serverCode,String userid, byte[] in) {
		String shaString = "";
		shaString = DigestUtils.sha384Hex(in);
		String userString = Base64Util.encode(userid);
		String serverString = DigestUtils.md5Hex(serverCode);
		String message = shaString + userString + serverString;
		return Character.toUpperCase(DigestUtils.sha3_224Hex(message).charAt(0));
	}
	
	/**
	 * In general, server will check the code first, and then search in database in order
	 * 	to keep from attack. TODO faster check function
	 * @param serverCode
	 * @param searchCode
	 * @param type
	 * @return
	 */
	public static boolean checkSearchCode(String serverCode, String searchCode, FileTypeRepository fileTypeRepository) {
		if (searchCode.length() != 15) {
			return false;
		}
		
		char codeA = searchCode.charAt(11);
		char codeB = searchCode.charAt(12);
		XFileType type = new XFileTypeConverter(fileTypeRepository).convert(searchCode.charAt(13));
		
		System.out.println("YOUR BUG!!!!!!!!!!!!!!!!!!"+type.name());
		
		long stamp = decodeTimestamp(searchCode.substring(0, 11));
		String stampString = String.valueOf(stamp);
		
		char[] searchc = stampString.toCharArray();
		short sum = 0;
		boolean isOdd = true;
		for (char c : searchc) {
			if(isOdd) {
				sum +=Short.valueOf(String.valueOf(c));
				System.out.print("-"+c+"-/"+sum+"/");
				isOdd = !isOdd;
			}else {
				sum +=(short)(Byte.valueOf(String.valueOf(c))*3);
				System.out.print("-"+c+"-/"+sum+"/");
				isOdd = !isOdd;
			}
		}
		sum += Short.valueOf(String.valueOf(codeA));
		System.out.println("length check passed"+codeA+"\r\n"+codeB+"\r\n"+String.valueOf(searchc)+"\r\n"
				+sum+"\r\n");
		if(sum % 10 != 0)return false;
		Timestamp timestamp = new Timestamp(stamp);
		String message = timestamp.toString()+type.name()+serverCode;
		System.out.println("mod check passed"+timestamp.getTime()+"\r\n"+message+"\r\n"+DigestUtils.sha384Hex(message).charAt(0));
		if(Character.toUpperCase(DigestUtils.sha384Hex(message).charAt(0)) != codeB)return false;
		return true;
	}
	
	
	public static String encodeTimestampHex(long stamp) {return Long.toHexString(stamp).toUpperCase();}
	public static String encodeTimeString(String stamp) {return encodeTimestampHex(Long.valueOf(stamp));}
	/**
	 * Turn Hex(length == 11) string into long. Use BigInteger for calculating.
	 * @param stamp Hex String
	 * @return the long value of hex.
	 */
	public static long decodeTimestamp(String stamp) {
		assert stamp.length() == 11;
		char[] chars = stamp.toCharArray();
		BigInteger result = BigInteger.valueOf(0l);
		for (int i = 0; i < chars.length; i++) {
			char c = chars[i];
			byte b;
			switch (c) {
			case 'A','a': b = (byte) 10;break;
			case 'B','b': b = (byte) 11;break;
			case 'C','c': b = (byte) 12;break;
			case 'D','d': b = (byte) 13;break;
			case 'E','e': b = (byte) 14;break;
			case 'F','f': b = (byte) 15;break;
			default: b = Byte.valueOf(String.valueOf(c));}
			//result += b * (16 ^ (chars.length - i-1)); too big
			result = result.add(BigInteger.valueOf(16l)
					.pow(chars.length - i-1)
					.multiply(BigInteger.valueOf(b)));
		}
		return result.longValueExact();
	}
	
	
	
	/**
	 * Mumbo-jumbo generator.
	 * @param user A created user included UUID, name, and timestamp.
	 * @param serverCode The serverSecureCode.
	 * @return a 32-character code (md5)
	 */
	public static String calculateUserCode(XUser user, String serverCode) {
		Random sr = new Random(XElementProperties.getSerialversionuid()
				+user.getProperties().getETimestamp().getTime());
		String userUUID1 = Long.toHexString(user.getProperties().getEUID());
		String userUUID[] = {
				userUUID1.substring(0, 4),
				userUUID1.substring(4, 8),
				userUUID1.substring(8, 12),
				userUUID1.substring(12,16)
		};
		String[] message = new String[5];
		XElementProperties tmpProperties = user.getProperties();
		message[0] = DigestUtils.sha512_256Hex(serverCode
				+userUUID[0]
				+sr.nextFloat());
		message[1] = DigestUtils.sha512_224Hex(userUUID[1]
				+tmpProperties.getETimestamp().toString()
				+serverCode
				+sr.nextFloat());
		message[2] = DigestUtils.sha3_512Hex(userUUID[2]
				+serverCode
				+sr.nextLong()
				+tmpProperties.getEType().name());
		message[3] = DigestUtils.sha3_384Hex(tmpProperties.getEName()
				+userUUID[3]
				+serverCode
				+sr.nextInt());
		message[4] = DigestUtils.sha3_224Hex(sr.nextInt()+XElementProperties.getSerialversionuid()+serverCode);
		StringBuffer stringBuffer = new StringBuffer();
		for(String s:message) {
			if(sr.nextBoolean()) {
				stringBuffer.append(s);
			}else {
				stringBuffer.insert(0, s);
			}
		}
		return DigestUtils.md5Hex(stringBuffer+String.valueOf(sr.nextInt()));
	}
	
	
	/**
	 * Turns 5 boolean values into 1 byte. Looks like (boolean)EODHS -> (byte) 00011111 / 00000000
	 * @param xProperties
	 * @return
	 */
	public static byte booleanPropToByte(XElementProperties xProperties) {
		//E-O-D-H-S
		byte result = 0;
		if (xProperties.isExist()) result += 16;
		if (xProperties.isOccupired()) result += 8;
		if (xProperties.isDestroyed()) result += 4;
		if (xProperties.isHidden()) result += 2;
		if (xProperties.isStashed()) result += 1;
		return result;
	}
	public static XElementProperties byteToBooleanProp(byte a, XElementProperties xProperties) {
		byte b = a;
		if (b > 15) {
			b -= 16;
			xProperties.setExist(true);
		}else {
			xProperties.setExist(false);
		}
		if (b > 7) {
			b -= 8;
			xProperties.setOccupired(true);
		}else {
			xProperties.setOccupired(false);
		}
		if (b > 3) {
			b -= 4;
			xProperties.setDestroyed(true);
		}else {
			xProperties.setDestroyed(false);
		}
		if (b > 1) {
			b -= 2;
			xProperties.setHidden(true);
		}else {
			xProperties.setHidden(false);
		}
		if (b > 0) {
			b -= 1;
			xProperties.setStashed(true);
		}else {
			xProperties.setStashed(false);
		}
		assert b == 0;
		return xProperties;
	}
	public static XElementProperties byteToBooleanProp(byte a) {
		return byteToBooleanProp(a,new XElementProperties());
	}
	
	public static Timestamp getTimeNow() {
		return new Timestamp(Timestamp.from(Instant.now()).getTime());
	}
}
