delete from Folder;
delete from File;
delete from XFileType_Ref;

delete from XFileType;
insert into XFileType (id, name, type)
    values ('A', 'application', 'application');
insert into XFileType (id, name, type)
    values ('B', 'audio', 'audio');
insert into XFileType (id, name, type)
    values ('C', 'font', 'font');
insert into XFileType (id, name, type)
    values ('D', 'image', 'image');
insert into XFileType (id, name, type)
    values ('E', 'message', 'message');
insert into XFileType (id, name, type)
    values ('F', 'model', 'model');
insert into XFileType (id, name, type)
    values ('G', 'multipart', 'multipart');
insert into XFileType (id, name, type)
    values ('H', 'text', 'text');
insert into XFileType (id, name, type)
    values ('I', 'video', 'video');
insert into XFileType (id, name, type)
    values ('X', 'undefined', 'undefined');
