CREATE TABLE IF NOT EXISTS Folder(
    id identity,
    ename varchar(50) not null,
    ealisa varchar(50),
    ebool tinyint not null,
    etime timestamp not null
);

create table if not exists File(
    id identity,
    folder bigint not null,
    folder_key bigint not null,
    ename varchar(50) not null,
    ealisa varchar(50),
    edesc varchar(500) not null,
    ebool tinyint not null,
    etime timestamp not null,
    data blob not null
);

create table if not exists XFileType_Ref (
    xfiletype char not null,
    file bigint not null,
    file_key bigint not null
);

create table if not exists XFileType (
    id char not null,
    name varchar(25) not null,
    type varchar(25) not null
);

alter table File
    add foreign key (folder) references Folder(id);
alter table XFileType_Ref
    add foreign key (file) references File(id);